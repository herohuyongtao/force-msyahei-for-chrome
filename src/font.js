'use strict';

function containInExceptions(t, exceptions) 
{
  var list = exceptions.split(/\r\n|\r|\n/);  // split at newline
  for (var i = 0; i < list.length; i++) {
    if (t.includes(list[i])) {
      return true;
    }
  }

  return false;
}

chrome.storage.sync.get({
  exceptions_user: '',
  exceptions_user_keyword: ''
}, function (obj) {
  if ((!obj.exceptions_user || !containInExceptions(location.hostname, obj.exceptions_user))
       && (!obj.exceptions_user_keyword || !containInExceptions(location.href, obj.exceptions_user_keyword))
     ) { // set for all websites excext the given exceptions
    var link = document.createElement("link");
    link.href = chrome.extension.getURL("font.css");
    link.type = "text/css";
    link.rel = "stylesheet";
    document.documentElement.insertBefore(link, null);
  }
});
