'use strict';

var log = document.getElementById('status');

function restore() {
  document.getElementById('exceptions_user').value = localStorage.getItem('exceptions_user') || '';
  document.getElementById('exceptions_user_keyword').value = localStorage.getItem('exceptions_user_keyword') || '';
}

function save() {
  localStorage.setItem('exceptions_user', document.getElementById('exceptions_user').value || '');
  localStorage.setItem('exceptions_user_keyword', document.getElementById('exceptions_user_keyword').value || '');

  const prefs = {
    exceptions_user: document.getElementById('exceptions_user').value,
    exceptions_user_keyword: document.getElementById('exceptions_user_keyword').value
  };

  chrome.storage.local.set(prefs, () => {
    log.textContent = 'Options saved.';
    setTimeout(() => log.textContent = '', 750);
    restore();

    chrome.storage.sync.set(
      {
        'exceptions_user': prefs.exceptions_user,
        'exceptions_user_keyword': prefs.exceptions_user_keyword
      }, function() {
      alert('saved');  // display string message
    });
  });
}

document.addEventListener('DOMContentLoaded', restore);
document.getElementById('save').addEventListener('click', () => {
  try {
    save();
  }
  catch (e) {
    log.textContent = e.message;
    setTimeout(() => log.textContent = '', 750);
  }
});
